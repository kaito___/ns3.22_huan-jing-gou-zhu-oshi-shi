/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

//
// Topology uses 3 APs on channels 1, 6, 11 placed on a line at 0m, 150m, 300m.
// They all have bridged CSMA interfaces that go to a L2 switch. A server also has a 
// link to the bridge. The mobile starts with 10m/s at 0m next to AP0, associates on channel 1   
// and starts receiving UDP data from the server till second 18.5. At 185m signal to AP0 is 
// quite weak and station scans all 11 channels, receiving Probe Responses from AP1 and AP2.   
// It chooses AP1 and stays on channel 6 till time 33.8s, and here only AP2 responds. Station uses 
// that link till second 47.5 (175m from AP2).  
//
// pcap dumps are:  wifi-5-1(station), wifi-0-2(AP0), wifi-1-2(AP1)
//
//    192.168.0.2      10m/s
//      --------     -------->            -------->           -------->       600m
//      WIFI STA@0m    
//      --------    
//        ((*))     
//                ((*))                       ((*))                ((*))
//              +---------+                +---------+          +----------+
//       .      | AP0(ch1)|@0m             | AP1(ch6)|@150m     | AP2(ch11)|@300m
//      /|\     |  BRIDGE |                |   BRIDGE|          |    BRIDGE|
//       |      +---------+                +---------+          +----------+
//       |            |                        |                   |
//       |            | CSMA                   | CSMA              | CSMA
//       |            |                        |                   |
//       |        +---+-----+                  |                   |
//       |        | Switch  |------------------+                   |
//       |        |         |--------------------------------------+         
//       |        +---+-----+             
//       |            | CSMA
//       |        +---+-----+  
//      UDP       | Server  | 192.168.1.1 
//                +---------+  

// dragos,niculescu at cs,pub,ro 

#include "ns3/core-module.h"
#include "ns3/mobility-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/network-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/bridge-helper.h"
#include "ns3/config-store-module.h"
//add
#include "ns3/netanim-module.h"
#include <vector>
#include <stdint.h>
#include <sstream>
#include <fstream>

using namespace ns3;





static void PrintPositions( std::string s, NodeContainer& staNodes)
{
  std::cout << "t = " << Simulator::Now ().GetMicroSeconds()/1000000.0 << " " << s << std::endl; 
    for (uint32_t i=0; i < staNodes.GetN(); i++ ){
      Ptr<MobilityModel> mob = staNodes.Get(i)->GetObject<MobilityModel>();
      Vector pos = mob->GetPosition ();
      std::cout  << i << ":" << "  " << pos.x << ", " << pos.y << std::endl;
    }
    Simulator::Schedule(Seconds(1), (&PrintPositions), s, staNodes);
}


static void
SetPosition (Ptr<Node> node, Vector position)
{
  Ptr<MobilityModel> mobility = node->GetObject<MobilityModel> ();
  mobility->SetPosition (position);
}

static void
SetPositionVelocity (Ptr<Node> node, Vector position, Vector velocity)
{
  Ptr<ConstantVelocityMobilityModel> mobility = node->GetObject<ConstantVelocityMobilityModel> ();
  mobility->SetPosition(position);
  mobility->SetVelocity(velocity);
}


void
CourseChanged (std::string context, Ptr<const MobilityModel> model)
{
  Vector position = model->GetPosition ();
  NS_LOG_UNCOND ( "At time " << Simulator::Now ().GetSeconds () << " "          << context << " x = " << position.x << ", y = " << position.y);
}



int main (int argc, char *argv[])
{
  uint32_t nWifis = 3;
  uint32_t nStas = 1;
  bool sendIp = true;
  bool writeMobility = true;
  bool pcap = true;

  CommandLine cmd;
  cmd.AddValue ("nWifis", "Number of wifi networks", nWifis);
  cmd.AddValue ("nStas", "Number of stations per wifi network", nStas);
  cmd.AddValue ("SendIp", "Send Ipv4 or raw packets", sendIp);
  cmd.AddValue ("writeMobility", "Write mobility trace", writeMobility);
  cmd.AddValue ("pcap", "Write pcap traces", pcap);
  cmd.Parse (argc, argv);

  Packet::EnablePrinting();

  // NODES 
  NodeContainer apNodes; 
  apNodes.Create (nWifis);
  Ptr<Node> serverNode =  CreateObject<Node> (), 
    serverNode2 =  CreateObject<Node> (),
    switchNode =  CreateObject<Node> ();
  std::vector<NodeContainer> staNodes;

  // DEVICES
  NetDeviceContainer *csmaDevices = new NetDeviceContainer[nWifis + 2]; // APs + serv
  Ipv4InterfaceContainer backboneInterfaces;
  std::vector<NetDeviceContainer> staDevices;
  std::vector<NetDeviceContainer> apDevices;
  std::vector<Ipv4InterfaceContainer> staInterfaces;
  std::vector<Ipv4InterfaceContainer> apInterfaces;

  InternetStackHelper stack;
  CsmaHelper csma, csma2;
  Ipv4AddressHelper ip;
  ip.SetBase ("192.168.0.0", "255.255.255.0");


  stack.Install (apNodes);
  stack.Install(serverNode); 
  stack.Install(serverNode2);
  stack.Install(switchNode); 
  
  // reduce csma queues if needed 
  // Config::SetDefault("ns3::DropTailQueue::MaxPackets", UintegerValue(10));

  for(uint32_t i = 0; i < nWifis; i++){ 
    csmaDevices[i] = csma.Install (NodeContainer(apNodes.Get(i), switchNode));
  }
  if(pcap)
    csma.EnablePcapAll ("csma", true);

  csmaDevices[nWifis] = csma2.Install (NodeContainer(serverNode, switchNode));
  csmaDevices[nWifis+1] = csma2.Install (NodeContainer(serverNode2, switchNode));
  if(pcap)
    csma2.EnablePcapAll ("csma", true);  
  
  BridgeHelper switch0;
  NetDeviceContainer switchDev; 
  for(uint32_t i = 0; i < nWifis + 2; i++){ 
    switchDev.Add(csmaDevices[i].Get(1)); 
  }
  switch0.Install(switchNode, switchDev); 

  Ipv4InterfaceContainer serverInterface;
  Ipv4InterfaceContainer serverInterface2;
  serverInterface = ip.Assign(csmaDevices[nWifis].Get(0));
  serverInterface2 = ip.Assign(csmaDevices[nWifis+1].Get(0)); 


  YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default ();
  wifiPhy.SetPcapDataLinkType (YansWifiPhyHelper::DLT_IEEE802_11_RADIO); 

  YansWifiChannelHelper wifiChannel = YansWifiChannelHelper::Default ();
  //wifiChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
  //wifiChannel.AddPropagationLoss("ns3::LogDistancePropagationLossModel");
  wifiPhy.SetChannel (wifiChannel.Create ());
  //wifiPhy.Set("ChannelNumber", UintegerValue(36));
  
  bool singleflag = true;//make single stanode
  for (uint32_t ap = 0; ap < nWifis; ++ap){
      std::ostringstream oss;
      oss << "wifi-default";
      Ssid ssid = Ssid (oss.str ());

      NodeContainer sta;
      NetDeviceContainer staDev;
      NetDeviceContainer apDev;
      Ipv4InterfaceContainer staInterface;
      Ipv4InterfaceContainer apInterface;
      MobilityHelper mobility;
      BridgeHelper bridge;
      WifiHelper wifi = WifiHelper::Default ();
      wifi.SetStandard (WIFI_PHY_STANDARD_80211g);
      // wifi.SetStandard (WIFI_PHY_STANDARD_80211n_2_4GHZ);
    
      NqosWifiMacHelper wifiMac = NqosWifiMacHelper::Default ();

 
      mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
                                     "MinX", DoubleValue (0.0),
                                     "MinY", DoubleValue (0.0),
                                     "DeltaX", DoubleValue (5.0),
                                     "DeltaY", DoubleValue (5.0),
                                     "GridWidth", UintegerValue (1),
                                     "LayoutType", StringValue ("RowFirst"));


      // setup the AP.
      mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
      mobility.Install (apNodes.Get (ap));
      wifiMac.SetType ("ns3::ApWifiMac",
                       "Ssid", SsidValue (ssid));
      // Config::SetDefault("ns3::WifiMacQueue::MaxPacketNumber", UintegerValue(20));
      wifiPhy.Set("ChannelNumber", UintegerValue(1 + (ap % 3) * 5)); 
      apDev = wifi.Install (wifiPhy, wifiMac, apNodes.Get (ap));

      NetDeviceContainer bridgeDev;
      bridgeDev = bridge.Install (apNodes.Get (ap), 
                                  NetDeviceContainer (apDev, csmaDevices[ap].Get (0)));
      if(singleflag){
        sta.Create (nStas);
        // setup the STAs
        stack.Install (sta);

        mobility.SetMobilityModel ("ns3::ConstantVelocityMobilityModel");
        mobility.Install (sta);

        wifiMac.SetType ("ns3::StaWifiMac",
                         "Ssid", SsidValue (ssid),
                         "ScanType", EnumValue (StaWifiMac::ACTIVE),
                         "ActiveProbing", BooleanValue (true));
        staDev = wifi.Install (wifiPhy, wifiMac, sta);
        staInterface = ip.Assign (staDev);
  
        // save everything in containers.
        staNodes.push_back (sta);
        staDevices.push_back (staDev);
        staInterfaces.push_back (staInterface);
        singleflag = false;
      }
      apDevices.push_back (apDev);
      apInterfaces.push_back (apInterface);
      
      SetPosition (apNodes.Get(ap),  Vector (ap*150.0, 20.0, 0.0));
  }

  //add extra stastions

  std::ostringstream oss;
      oss << "wifi-default";
      Ssid ssid = Ssid (oss.str ());

  MobilityHelper mobility;
  BridgeHelper bridge;
  WifiHelper wifi = WifiHelper::Default ();
  wifi.SetStandard (WIFI_PHY_STANDARD_80211g);
  // wifi.SetStandard (WIFI_PHY_STANDARD_80211n_2_4GHZ);
  Ipv4InterfaceContainer staInterface;

  NqosWifiMacHelper wifiMac = NqosWifiMacHelper::Default ();

  int extraNumber = 2;
  NodeContainer extraSta1;
  NodeContainer extraSta2;
  extraSta1.Create(extraNumber);
  extraSta2.Create(extraNumber);
  NetDeviceContainer extraStaDev1;
  NetDeviceContainer extraStaDev2;
  mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
  mobility.Install (extraSta1);
  mobility.Install (extraSta2);
  for(uint8_t i = 0;i<extraSta1.GetN();i++){
    SetPosition(extraSta1.Get(i),Vector(150,(3*i),0));
    SetPosition(extraSta2.Get(i),Vector(300,(3*i),0));
  }
  wifiMac.SetType ("ns3::StaWifiMac",
                   "Ssid", SsidValue (ssid),
                   "ScanType", EnumValue (StaWifiMac::ACTIVE),
                   "ActiveProbing", BooleanValue (true));
  wifiPhy.Set("ChannelNumber", UintegerValue(1 + (1 % 3) * 5));
  extraStaDev1 = wifi.Install (wifiPhy, wifiMac, extraSta1);
  wifiPhy.Set("ChannelNumber", UintegerValue(1 + (2 % 3) * 5));
  extraStaDev2 = wifi.Install (wifiPhy, wifiMac, extraSta2);
  stack.Install (extraSta1);
  stack.Install (extraSta2);
  staInterface = ip.Assign (extraStaDev1);
  staInterface = ip.Assign (extraStaDev2);


  // first node moves to the left
  SetPositionVelocity (staNodes[0].Get(0),  Vector (0.0, 0.0, 0.0), (Vector(10.0, 0.0, 0.0)));
  //  SetPositionVelocity (staNodes[1].Get(0),  Vector (100.0, 9.0, 0.0), (Vector(-1.0, 0.0, 0.0)));
  
  Simulator::Schedule(Seconds(0), (&PrintPositions), "ap0", staNodes[0]);
     
  if (writeMobility){
    AsciiTraceHelper ascii;
    MobilityHelper::EnableAsciiAll (ascii.CreateFileStream ("wifi-wired-bridging.mob"));
  }

  if(pcap){
    //wifiPhy.EnablePcapAll ("pcap/wifi");
    wifiPhy.EnablePcap ("pcap/sta",staNodes[0].Get(0)->GetId(),true);
    wifiPhy.EnablePcap ("pcap/ap",apNodes,true);
  }



  UdpServerHelper srv(9);  
  ApplicationContainer srv_apps = srv.Install (staNodes[0].Get(0)); //server = UDP recv 
  ApplicationContainer srv_apps2 = srv.Install (serverNode2); //server = UDP recv 
  srv_apps.Start (Seconds (0.5));
  srv_apps.Stop (Seconds (60.0));
  srv_apps2.Start (Seconds (0.5));
  srv_apps2.Stop (Seconds (60.0));
  UdpClientHelper client(staInterfaces[0].GetAddress (0), 9); // dest: IP,port
  client.SetAttribute("MaxPackets",UintegerValue (64707202));
  client.SetAttribute("Interval",TimeValue (Time ("0.1"))); 
  client.SetAttribute("PacketSize",UintegerValue (1450));    
  ApplicationContainer cln_apps = client.Install (serverNode); //cli = UDP send
  cln_apps.Start (Seconds (0.4));
  cln_apps.Stop (Seconds (60.0));

  //add extra echo
  UdpClientHelper client2(serverInterface2.GetAddress(0), 9); // dest: IP,port
  client2.SetAttribute("MaxPackets",UintegerValue (64707202));
  client2.SetAttribute("Interval",TimeValue (Time ("0.01"))); 
  client2.SetAttribute("PacketSize",UintegerValue (1450));
  ApplicationContainer cln_apps2 = client2.Install (extraSta1);

  UdpClientHelper client3(serverInterface2.GetAddress(0), 9); // dest: IP,port
  client3.SetAttribute("MaxPackets",UintegerValue (64707202));
  client3.SetAttribute("Interval",TimeValue (Time ("0.01"))); 
  client3.SetAttribute("PacketSize",UintegerValue (1450));
  ApplicationContainer cln_apps3 = client2.Install (extraSta2);
  cln_apps2.Start (Seconds (0.5));
  cln_apps2.Stop (Seconds (60.0));
  cln_apps3.Start (Seconds (0.5));
  cln_apps3.Stop (Seconds (60.0));

  
/*
  //add animation config
  AnimationInterface::SetConstantPosition (serverNode, 100, 150); 
  AnimationInterface::SetConstantPosition (serverNode2, 120, 150); 
  AnimationInterface::SetConstantPosition (switchNode, 100, 100); 

  AnimationInterface anim ("a_mywifi1_animation.xml"); // Mandatory

  for (uint32_t i = 0; i < staNodes.size(); ++i)
    {
      for (uint32_t m = 0; m < staNodes[i].GetN(); ++m)
      {
        anim.UpdateNodeDescription (staNodes[i].Get(m), "STA"); // Optional
        anim.UpdateNodeColor (staNodes[i].Get(m), 255, 0, 0); // Optional
      }
    }
  for (uint32_t i = 0; i < apNodes.GetN (); ++i)
    {
      anim.UpdateNodeDescription (apNodes.Get (i), "AP"); // Optional
      anim.UpdateNodeColor (apNodes.Get (i), 0, 255, 0); // Optional
    }
  anim.UpdateNodeColor (serverNode, 255, 255, 0); // Optional
  anim.UpdateNodeColor (switchNode, 0, 0, 0); // Optional
  anim.EnablePacketMetadata (); // Optional
*/

  // Trace routing tables 
/*
  Ipv4GlobalRoutingHelper g;
  Ptr<OutputStreamWrapper> routingStream = Create<OutputStreamWrapper> ("routes.txt", std::ios::out);
  g.PrintRoutingTableAllAt (Seconds (4.0), routingStream);
*/
  // print config
  Config::SetDefault ("ns3::ConfigStore::Filename", StringValue ("output-attributes.txt"));
  Config::SetDefault ("ns3::ConfigStore::FileFormat", StringValue ("RawText"));
  Config::SetDefault ("ns3::ConfigStore::Mode", StringValue ("Save"));
  ConfigStore outputConfig2;
  outputConfig2.ConfigureDefaults ();
  outputConfig2.ConfigureAttributes ();

  
  
  Simulator::Stop (Seconds (60.0));
  Simulator::Run ();
  Simulator::Destroy ();
}