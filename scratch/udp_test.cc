#include "ns3/core-module.h"
#include "ns3/mobility-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/network-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/bridge-helper.h"
#include "ns3/config-store-module.h"
#include "ns3/packet.h"
#include <vector>
#include <stdint.h>
#include <sstream>
#include <fstream>

using namespace ns3;


int main (int argc, char *argv[])
{
  // NODES 
  /*
  Ptr<Node> serverNode =  CreateObject<Node> ();
  Ptr<Node> clientNode =  CreateObject<Node> ();
  */
  NodeContainer nodes;
  nodes.Create(2);

  CsmaHelper csma;
  csma.SetChannelAttribute ("DataRate", DataRateValue (DataRate (5000000)));
  csma.SetChannelAttribute ("Delay", TimeValue (MilliSeconds (2)));
  csma.SetDeviceAttribute ("Mtu", UintegerValue (1400));
  NetDeviceContainer d = csma.Install (nodes);

  InternetStackHelper internet;
  internet.Install (nodes);

  Address serverAddress;
  Ipv4AddressHelper ipv4;
  ipv4.SetBase ("10.1.1.0", "255.255.255.0");
  Ipv4InterfaceContainer i = ipv4.Assign (d);
  serverAddress = Address(i.GetAddress (0));
  //serverAddress = Address(nodes.Get(0)->GetObject<Ipv4Interface> ()->GetAddress(0));


  std::cout<<i.GetAddress(0)<<std::endl;
  std::cout<<i.GetAddress(1)<<std::endl;
  std::cout<<Address(i.GetAddress(0))<<std::endl;
  std::cout<<Address(i.GetAddress(1))<<std::endl;

  uint16_t port = 9;  // well-known echo port number
  UdpEchoServerHelper server (port);
  ApplicationContainer apps = server.Install (nodes.Get(0));
  apps.Start (Seconds (1.0));
  apps.Stop (Seconds (10.0));

  uint32_t packetSize = 1024;
  uint32_t maxPacketCount = 1;
  Time interPacketInterval = Seconds (1.);
  UdpEchoClientHelper client (serverAddress, port);
  client.SetAttribute ("MaxPackets", UintegerValue (maxPacketCount));
  client.SetAttribute ("Interval", TimeValue (interPacketInterval));
  client.SetAttribute ("PacketSize", UintegerValue (packetSize));
  apps = client.Install (nodes.Get(1));
  apps.Start (Seconds (2.0));
  apps.Stop (Seconds (10.0));
  
  Packet::EnablePrinting();

  csma.EnablePcapAll ("udp_test_pcap/udp-echo", false);

  //Simulator::Stop (Seconds (10.0));
  Simulator::Run ();
  Simulator::Destroy ();
}