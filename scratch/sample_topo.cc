#include "ns3/core-module.h"
#include "ns3/mobility-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/network-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/bridge-helper.h"
#include "ns3/config-store-module.h"
//add
#include "ns3/netanim-module.h"
#include <vector>
#include <stdint.h>
#include <sstream>
#include <fstream>

using namespace ns3;



int main (int argc, char *argv[])
{
  // NODES
  NodeContainer hostNodes;
  Ptr<Node> serverNode =  CreateObject<Node> ();
  Ptr<Node> swNode =  CreateObject<Node> ();
  hostNodes.Add(serverNode);
  hostNodes.Create(3);

  InternetStackHelper stack;
  stack.Install(hostNodes);

  CsmaHelper csmaHelper;
  csmaHelper.SetChannelAttribute("DataRate",StringValue("100Mbps"));
  csmaHelper.SetChannelAttribute("Delay", TimeValue(MilliSeconds(2)));
  NetDeviceContainer hostDevs, swDevs;


  for (uint32_t i = 0;i<hostNodes.GetN();i++){
    NetDeviceContainer link = csmaHelper.Install(NodeContainer(hostNodes.Get(i),swNode));
    hostDevs.Add(link.Get(0));
    swDevs.Add(link.Get(1));
  }

  BridgeHelper switchHelper;
  /*
  switchHelper.SetDeviceAttribute("Mtu", UintegerValue(1500));
  switchHelper.SetDeviceAttribute("EnableLearning", BooleanValue(true));
  switchHelper.SetDeviceAttribute("ExpirationTime", TimeValue(Seconds(300)));
  */
  switchHelper.Install(swNode,swDevs);

  

  Ipv4AddressHelper ipv4;
  ipv4.SetBase("10.1.1.0","255.2555.255.0");
  Ipv4InterfaceContainer hostInterfaces = ipv4.Assign(hostDevs);

  

  UdpEchoServerHelper srv(9);
  ApplicationContainer srv_apps = srv.Install (serverNode); //server = UDP recv
  srv_apps.Start (Seconds (0.5));
  srv_apps.Stop (Seconds (60.0));

  //std::cout<<lan1Interfaces.GetAddress(2)<<std::endl;
  //Address serverAddress = Address(hostInterfaces.GetAddress(0));
  UdpEchoClientHelper client(hostInterfaces.GetAddress(0), 9); // dest: IP,port
  Time interPacketInterval = Seconds (1.);
  client.SetAttribute("MaxPackets",UintegerValue (100));
  client.SetAttribute("Interval",TimeValue (interPacketInterval));
  client.SetAttribute("PacketSize",UintegerValue (1050));
  ApplicationContainer cln_apps = client.Install (hostNodes.Get(1)); //cli = UDP send
  cln_apps.Start (Seconds (5));
  cln_apps.Stop (Seconds (60.0));

  ////////////////////////////////////////////////////////////////////////////////////////


  //Ipv4GlobalRoutingHelper::PopulateRoutingTables();

/*
  // print config
  Config::SetDefault ("ns3::ConfigStore::Filename", StringValue ("output-attributes.txt"));
  Config::SetDefault ("ns3::ConfigStore::FileFormat", StringValue ("RawText"));
  Config::SetDefault ("ns3::ConfigStore::Mode", StringValue ("Save"));
  ConfigStore outputConfig2;
  outputConfig2.ConfigureDefaults ();
  outputConfig2.ConfigureAttributes ();
*/

  csmaHelper.EnablePcapAll("sample_topo_pcap/csma",false);

  Simulator::Stop (Seconds (30.0));
  Simulator::Run ();
  Simulator::Destroy ();
}
