#include "ns3/core-module.h"
#include "ns3/mobility-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/network-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/bridge-helper.h"
#include "ns3/config-store-module.h"
//add
#include "ns3/netanim-module.h"
#include <vector>
#include <stdint.h>
#include <sstream>
#include <fstream>

using namespace ns3;

void PrintRoutingTable(Ptr<Node>& n){
  Ptr<Ipv4StaticRouting> routing = 0;
  Ipv4StaticRoutingHelper routingHelper;
  Ptr<Ipv4> ipv4 = n->GetObject<Ipv4>();
  uint32_t nbRoutes = 0;
  Ipv4RoutingTableEntry route;

  routing = routingHelper.GetStaticRouting(ipv4);

  nbRoutes = routing->GetNRoutes();

  NS_LOG_UNCOND("Routing table of"<< n << " :");
  NS_LOG_UNCOND("----------------------------");
  std::cout << "Destination\t"
            << "Network mask\t"
            << "Gateway \t"
            << "I/F\n";

  for (uint32_t i = 0; i < nbRoutes; i++){
    route = routing->GetRoute(i);
    std::cout << route.GetDestNetwork() << "\t"
              << route.GetDestNetworkMask() << "\t"
              << route.GetGateway() << "\t\t"
              << route.GetInterface() << std::endl;
  }
}


int main (int argc, char *argv[])
{
  // NODES
  NodeContainer stackNodes;
  Ptr<Node> routerNode =  CreateObject<Node> ();
  NodeContainer hostNodes1;
  hostNodes1.Create(3);
  NodeContainer lan1;
  Ptr<Node> serverNode1 =  CreateObject<Node> ();
  Ptr<Node> swNode1 =  CreateObject<Node> ();
  lan1.Add(serverNode1);
  lan1.Add(routerNode);
  lan1.Add(hostNodes1);


  NodeContainer hostNodes2;
  hostNodes2.Create(3);
  NodeContainer lan2;
  Ptr<Node> serverNode2 =  CreateObject<Node> ();
  Ptr<Node> swNode2 =  CreateObject<Node> ();
  lan2.Add(serverNode2);
  lan2.Add(routerNode);
  lan2.Add(hostNodes2);
  

  InternetStackHelper stack;
  stackNodes.Add(lan1);
  stackNodes.Add(hostNodes2);
  stackNodes.Add(serverNode2);
  stack.Install(stackNodes);

  CsmaHelper csmaHelper;
  csmaHelper.SetChannelAttribute("DataRate",StringValue("100Mbps"));
  csmaHelper.SetChannelAttribute("Delay", TimeValue(MilliSeconds(2)));
  NetDeviceContainer hostDevs1, swDevs1, hostDevs2, swDevs2;


  for (uint32_t i = 0;i<lan1.GetN();i++){
    NetDeviceContainer link1 = csmaHelper.Install(NodeContainer(lan1.Get(i),swNode1));
    hostDevs1.Add(link1.Get(0));
    swDevs1.Add(link1.Get(1));
  }

  for (uint32_t i = 0;i<lan1.GetN();i++){
    NetDeviceContainer link2 = csmaHelper.Install(NodeContainer(lan2.Get(i),swNode2));
    hostDevs2.Add(link2.Get(0));
    swDevs2.Add(link2.Get(1));
  }

  BridgeHelper switchHelper;
  /*
  switchHelper.SetDeviceAttribute("Mtu", UintegerValue(1500));
  switchHelper.SetDeviceAttribute("EnableLearning", BooleanValue(true));
  switchHelper.SetDeviceAttribute("ExpirationTime", TimeValue(Seconds(300)));
  */
  switchHelper.Install(swNode1,swDevs1);
  switchHelper.Install(swNode2,swDevs2);
  

  Ipv4AddressHelper ipv4;
  ipv4.SetBase("10.1.1.0","255.255.255.0");
  Ipv4InterfaceContainer hostInterfaces1 = ipv4.Assign(hostDevs1);
  ipv4.SetBase("10.1.2.0","255.255.255.0");
  Ipv4InterfaceContainer hostInterfaces2 = ipv4.Assign(hostDevs2);

  

  UdpEchoServerHelper srv(9);
  ApplicationContainer srv_apps = srv.Install (lan2.Get(1)); //server = UDP recv
  srv_apps.Start (Seconds (0.5));
  srv_apps.Stop (Seconds (60.0));

  std::cout<<hostInterfaces2.GetAddress(0)<<std::endl;
  //Address serverAddress = Address(hostInterfaces.GetAddress(0));
  UdpEchoClientHelper client(hostInterfaces2.GetAddress(0), 9); // dest: IP,port
  Time interPacketInterval = Seconds (1.);
  client.SetAttribute("MaxPackets",UintegerValue (100));
  client.SetAttribute("Interval",TimeValue (interPacketInterval));
  client.SetAttribute("PacketSize",UintegerValue (1050));
  ApplicationContainer cln_apps = client.Install (lan1.Get(2)); //cli = UDP send
  cln_apps.Start (Seconds (5));
  cln_apps.Stop (Seconds (60.0));


  //set default gateway
  Ipv4Address gateway1("10.1.1.2");
  Ipv4StaticRoutingHelper helper;
  Ptr<Ipv4StaticRouting> r;
  for(uint32_t i = 0;i < lan1.GetN();i++){
    if(i!=1){
      r = helper.GetStaticRouting(lan1.Get(i)->GetObject<Ipv4>());
      r->SetDefaultRoute(gateway1,1);
    }
  }
  Ipv4Address gateway2("10.1.2.2");
  for(uint32_t i = 0;i < lan2.GetN();i++){
    if(i!=1){
      r = helper.GetStaticRouting(lan2.Get(i)->GetObject<Ipv4>());
      r->SetDefaultRoute(gateway2,1);
    }
  }
  /*
  r = helper.GetStaticRouting(lan1.Get(2)->GetObject<Ipv4>());
  r->SetDefaultRoute(gateway,1);
  */

  ////////////////////////////////////////////////////////////////////////////////////////


  Ipv4GlobalRoutingHelper::PopulateRoutingTables();

  PrintRoutingTable(routerNode);
/*
  // print config
  Config::SetDefault ("ns3::ConfigStore::Filename", StringValue ("output-attributes.txt"));
  Config::SetDefault ("ns3::ConfigStore::FileFormat", StringValue ("RawText"));
  Config::SetDefault ("ns3::ConfigStore::Mode", StringValue ("Save"));
  ConfigStore outputConfig2;
  outputConfig2.ConfigureDefaults ();
  outputConfig2.ConfigureAttributes ();
*/

  MobilityHelper mobility;
  mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
  mobility.Install(stackNodes);
  mobility.Install(swNode1);
  mobility.Install(swNode2);

  //add animation config
  AnimationInterface::SetConstantPosition (serverNode1, 20, 20); 
  AnimationInterface::SetConstantPosition (serverNode2, 100, 20); 
  AnimationInterface::SetConstantPosition (swNode1, 30, 30); 
  AnimationInterface::SetConstantPosition (swNode2, 90, 30); 
  AnimationInterface::SetConstantPosition (routerNode, 60, 20); 


  AnimationInterface anim ("sample_topo_pcap/animation.xml"); // Mandatory

  
  for (uint32_t i = 0; i < hostNodes1.GetN (); ++i)
    {
      AnimationInterface::SetConstantPosition (hostNodes1.Get (i), i*10, 50);
      anim.UpdateNodeColor (hostNodes1.Get (i), 0, 255, 0); // Optional
    }
  for (uint32_t i = 0; i < hostNodes2.GetN (); ++i)
    {
      AnimationInterface::SetConstantPosition (hostNodes2.Get (i), 90+i*10, 50);
      anim.UpdateNodeColor (hostNodes2.Get (i), 0, 255, 0); // Optional
    }
  anim.UpdateNodeColor (serverNode1, 255, 255, 0); // Optional
  anim.UpdateNodeColor (serverNode2, 255, 255, 0); // Optional
  anim.UpdateNodeColor (swNode1, 0, 0, 0); // Optional
  anim.UpdateNodeColor (swNode2, 0, 0, 0); // Optional
  anim.UpdateNodeColor (routerNode, 255, 0, 128); // Optional
  anim.EnablePacketMetadata (); // Optional

  csmaHelper.EnablePcapAll("sample_topo_pcap/csma",false);

  Simulator::Stop (Seconds (70.0));
  Simulator::Run ();
  Simulator::Destroy ();
}
